﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;
using System.Drawing;

namespace WPFSerialAssistant
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            InitCore();
            InitzedGraph_speed();
        }

        

        private void InitzedGraph_speed()
        {
            this.zedGraph_speed.GraphPane.Chart.Fill = new Fill(System.Drawing.Color.Transparent, System.Drawing.Color.Transparent, 45f);
            this.zedGraph_speed.MasterPane.Fill = new Fill(System.Drawing.Color.Transparent, System.Drawing.Color.Transparent, 45f);
            this.zedGraph_speed.GraphPane.Fill.Color = System.Drawing.Color.Transparent;
            ZedGraph.Border border = new ZedGraph.Border(false, System.Drawing.Color.Black, 10);
            //设置曲线标签的边框
            this.zedGraph_speed.GraphPane.Legend.Border = border;
            this.zedGraph_speed.GraphPane.Legend.Fill.Color = System.Drawing.Color.Transparent;
            this.zedGraph_speed.GraphPane.Chart.Border.Color = System.Drawing.Color.SteelBlue;
            this.zedGraph_speed.GraphPane.Border.Color = System.Drawing.Color.White;
            this.zedGraph_speed.GraphPane.XAxis.Scale.FontSpec.FontColor = System.Drawing.Color.SteelBlue;

            //图表X轴刻度大刻度的颜色
            this.zedGraph_speed.GraphPane.XAxis.MajorTic.Color = System.Drawing.Color.SpringGreen;
            //图表X轴刻度小刻度的颜色
            this.zedGraph_speed.GraphPane.YAxis.MinorGrid.IsVisible = false;
            this.zedGraph_speed.GraphPane.XAxis.MinorGrid.IsVisible = false;
            this.zedGraph_speed.GraphPane.XAxis.MinorTic.Color = System.Drawing.Color.SpringGreen;
            this.zedGraph_speed.GraphPane.YAxis.Scale.FontSpec.FontColor = System.Drawing.Color.LightBlue;
            //图表Y轴刻度大刻度的颜色
            this.zedGraph_speed.GraphPane.YAxis.MajorTic.Color = System.Drawing.Color.SpringGreen;
            //图表Y轴刻度小刻度的颜色
            this.zedGraph_speed.GraphPane.YAxis.MinorTic.Color = System.Drawing.Color.SpringGreen;

            zedGraph_speed.GraphPane.Title.Text = "速度曲线";
            zedGraph_speed.GraphPane.XAxis.Title.Text = " ";
            zedGraph_speed.GraphPane.YAxis.Title.Text = "速度";
            zedGraph_speed.GraphPane.Title.FontSpec.FontColor = System.Drawing.Color.Red;
            zedGraph_speed.GraphPane.XAxis.Title.FontSpec.Family = "微软雅黑";
            zedGraph_speed.GraphPane.XAxis.Title.FontSpec.Size = 18;
            zedGraph_speed.GraphPane.XAxis.Title.FontSpec.IsBold = false;
            zedGraph_speed.GraphPane.XAxis.Title.FontSpec.FontColor = System.Drawing.Color.Black;
            zedGraph_speed.GraphPane.YAxis.Title.FontSpec.FontColor = System.Drawing.Color.ForestGreen;

            zedGraph_speed.GraphPane.YAxis.Title.FontSpec.Family = "微软雅黑";
            zedGraph_speed.GraphPane.YAxis.Title.FontSpec.Size = 16;
            zedGraph_speed.GraphPane.YAxis.Title.FontSpec.IsBold = false;

            //将X、Y轴的对面坐标轴隐藏
            zedGraph_speed.GraphPane.XAxis.MajorTic.IsOpposite = false;
            zedGraph_speed.GraphPane.YAxis.MajorTic.IsOpposite = false;
            zedGraph_speed.GraphPane.YAxis.MinorTic.IsOpposite = false;
            zedGraph_speed.GraphPane.XAxis.MinorTic.IsOpposite = false;


            // zedGraphControl1.GraphPane.XAxis.Type = AxisType.Text;
            //显示小刻度 是false则看不到效果
            zedGraph_speed.GraphPane.XAxis.MinorGrid.IsVisible = false;
            //线的颜色
            //zedGraphControl1.GraphPane.XAxis.Color = System.Drawing.Color.Gray;
            //点线中点与点之间的间隔
            zedGraph_speed.GraphPane.XAxis.MinorGrid.DashOff = 1f;
            //点线中点的长度
            zedGraph_speed.GraphPane.XAxis.MinorGrid.DashOn = 1f;
            //画笔宽度
            zedGraph_speed.GraphPane.XAxis.MinorGrid.PenWidth = 2f;
            zedGraph_speed.GraphPane.Fill = new Fill(System.Drawing.Color.White, System.Drawing.Color.SpringGreen, 45.0f);
            //zedGraph_speed.GraphPane.XAxis.Scale.Min = 0;              //X轴最小值0           
            //zedGraph_speed.GraphPane.XAxis.Scale.Max = 10;
            //zedGraph_speed.GraphPane.XAxis.Scale.MinorStep = 1;
            //zedGraph_speed.GraphPane.XAxis.Scale.MajorStep = 100;
            //zedGraph_speed.GraphPane.YAxis.Scale.Min = 0;
            //zedGraph_speed.GraphPane.YAxis.Scale.Max = 20;           //这里要改成动态的，大小随波形改变
            //zedGraph_speed.GraphPane.YAxis.Scale.MinorStep = 1;
            //zedGraph_speed.GraphPane.YAxis.Scale.MajorStep = 100;
            zedGraph_speed.AxisChange();
            //showChart1();
        }
    }
}


