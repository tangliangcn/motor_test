﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace WPFSerialAssistant
{
    public partial class MainWindow : Window
    {
        /// <summary>
        /// 用于更新时间的定时器
        /// </summary>
        private DispatcherTimer clockTimer = new DispatcherTimer();

        private DispatcherTimer updateTimer = new DispatcherTimer();

        /// <summary>
        /// 定时器初始化
        /// </summary>
        private void InitClockTimer()
        {
            clockTimer.Interval = new TimeSpan(0, 0, 1);
            clockTimer.IsEnabled = true;
            clockTimer.Tick += ClockTimer_Tick;
            clockTimer.Start();
        }

        private void InitUpdateTimer()
        {
            updateTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            updateTimer.IsEnabled = true;
            updateTimer.Tick += updateTimer_Tick;
            //updateTimer.Start();
        }



    }
}
