﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Windows.Documents;
using Microsoft.Win32;
using ZedGraph;
using Microsoft.Office.Interop.Excel;

namespace WPFSerialAssistant
{
    public partial class MainWindow : System.Windows.Window
    {

        private string GetSaveDataPath()
        {
            string path = @"data.xls";

            SaveFileDialog sfd = new SaveFileDialog();

            sfd.Title = "选择存储数据的路径...";
            sfd.FileName = string.Format("数据{0}", DateTime.Now.ToString("yyyyMdHHMMss"));
            sfd.Filter = "Excel表格|*.xls";

            if (sfd.ShowDialog() == true)
            {
                path = sfd.FileName;
            }
            return path;
        }

        private void SaveData(string path)
        {
            List<string> listName = new List<string>();
            listName.Add("序号");
            listName.Add("CH1");
            listName.Add("CH2");
            listName.Add("CH3");
            listName.Add("CH4");
            //将listView1中的数据保存到excel表格中
            object misValue = System.Reflection.Missing.Value;
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            Workbook xlWorkBook = xlApp.Workbooks.Add(misValue);
            Worksheet xlWorkSheet = (Worksheet)xlWorkBook.Worksheets.get_Item(1);
            for (int i = 0; i < listName.Count; i++)
            {
                xlWorkSheet.Cells[1, i + 1] = listName[i]; //write the column name
            }
            for (int i = 0; i < list_ch1.Count; i++)
            {
                //listView1.Items[i].Selected = true;//触发选中第一行 
                xlWorkSheet.Cells[i + 2, 1] = list_ch1[i].X.ToString();//第i行第一列
                xlWorkSheet.Cells[i + 2, 2] = list_ch1[i].Y.ToString();//第i行第二列
                xlWorkSheet.Cells[i + 2, 3] = list_ch2[i].Y.ToString();
                xlWorkSheet.Cells[i + 2, 4] = list_ch3[i].Y.ToString();//第i行第二列
                xlWorkSheet.Cells[i + 2, 5] = list_ch4[i].Y.ToString();
            }
            try
            {
                //SaveFileDialog sfd = new SaveFileDialog();
                //sfd.InitialDirectory = Directory.GetCurrentDirectory() + "\\data\\";
                //sfd.Filter = "Excel表格|*.xls";
                //sfd.ShowDialog();
                //string path = sfd.FileName;
                xlWorkBook.SaveAs(path, XlFileFormat.xlExcel7, misValue, misValue, misValue, misValue, XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();
                MessageBox.Show("保存成功！");
            }
            catch (Exception)
            {

            }


            //try
            //{
            //    using (System.IO.StreamWriter sr = new StreamWriter(path))
            //    {
            //        //System.IO.File.WriteAllText(path, "CH1" + list_ch1.ToString(), Encoding.Default);

            //        foreach ( PointPair i in list_ch1)
            //        {
            //            sr.WriteLine(i.Y.ToString());
            //        }

            //        Information(string.Format("成功保存数据到{0}", path));
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Alert(ex.Message);
            //}
        }
        }
}
