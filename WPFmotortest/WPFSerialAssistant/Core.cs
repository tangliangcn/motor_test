﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace WPFSerialAssistant
{
    public partial class MainWindow : Window
    {
        /// <summary>
        /// 核心初始化
        /// </summary>
        private void InitCore()
        {
            // 加载配置信息
            LoadConfig();

            // 其他模块初始化
            InitClockTimer();
            InitUpdateTimer();
            InitSerialPort();

            // 查找可以使用的端口号
            FindPorts();
        }

        #region 状态栏
        /// <summary>
        /// 更新时间信息
        /// </summary>
        private void UpdateTimeDate()
        {
            string timeDateString = "";
            DateTime now = DateTime.Now;
            timeDateString = string.Format("{0}年{1}月{2}日 {3}:{4}:{5}", 
                now.Year, 
                now.Month.ToString("00"), 
                now.Day.ToString("00"), 
                now.Hour.ToString("00"), 
                now.Minute.ToString("00"), 
                now.Second.ToString("00"));

            timeDateTextBlock.Text = timeDateString;
        }

        /// <summary>
        /// 警告信息提示（一直提示）
        /// </summary>
        /// <param name="message">提示信息</param>
        private void Alert(string message)
        {
            // #FF68217A
            statusBar.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x21, 0x2A));
            statusInfoTextBlock.Text = message;
        }

        /// <summary>
        /// 普通状态信息提示
        /// </summary>
        /// <param name="message">提示信息</param>
        private void Information(string message)
        {
            if (serialPort.IsOpen)
            {
                // #FFCA5100
                statusBar.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xCA, 0x51, 0x00));
            }
            else
            {
                // #FF007ACC
                statusBar.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x00, 0x7A, 0xCC));
            }
            statusInfoTextBlock.Text = message;
        }

        #endregion

        

        #region 配置信息
        // 
        // 目前保存的配置信息如下：
        // 1. 波特率
        // 2. 奇偶校验位
        // 3. 数据位
        // 4. 停止位
        // 5. 字节编码
        // 6. 发送区文本内容
        // 7. 自动发送时间间隔
        // 8. 窗口状态：最大化|高度+宽度
        // 9. 面板显示状态
        // 10. 接收数据模式
        // 11. 是否显示接收数据
        // 12. 发送数据模式
        // 13. 发送追加内容
        //

        /// <summary>
        /// 保存配置信息
        /// </summary>
        private void SaveConfig()
        {
            // 配置对象实例
            Configuration config = new Configuration();

            // 保存波特率
            AddBaudRate(config);

            // 保存奇偶校验位
            config.Add("parity", parityComboBox.SelectedIndex);

            // 保存数据位
            config.Add("dataBits", dataBitsComboBox.SelectedIndex);

            // 保存停止位
            config.Add("stopBits", stopBitsComboBox.SelectedIndex);

            // 字节编码
            config.Add("encoding", encodingComboBox.SelectedIndex);

            // 窗口状态信息
            config.Add("maxmized", this.WindowState == WindowState.Maximized);  
            config.Add("windowWidth", this.Width);
            config.Add("windowHeight", this.Height);
            config.Add("windowLeft", this.Left);
            config.Add("windowTop", this.Top);

            // 面板显示状态
            config.Add("serialPortConfigPanelVisible", serialPortConfigPanel.Visibility == Visibility.Visible);
            //config.Add("autoSendConfigPanelVisible", autoSendConfigPanel.Visibility == Visibility.Visible);
            config.Add("serialCommunicationConfigPanelVisible", serialCommunicationConfigPanel.Visibility == Visibility.Visible);


            // 保存发送模式
            config.Add("sendMode", sendMode);

            // 保存参数
            config.Add("CMD1", CMD1_TB.Text);
            config.Add("CMD2", CMD2_TB.Text);
            config.Add("CMD3", CMD3_TB.Text);
            config.Add("CMD4", CMD4_TB.Text);
            config.Add("CMD5", CMD5_TB.Text);
            config.Add("CMD6", CMD6_TB.Text);
            config.Add("CMD7", CMD7_TB.Text);
            config.Add("CMD8", CMD8_TB.Text);
            config.Add("CMD9", CMD9_TB.Text);
            config.Add("CMD10", CMD10_TB.Text);
            config.Add("CMD11", CMD11_TB.Text);
            config.Add("CMD12", CMD12_TB.Text);

            // 保存配置信息到磁盘中
            Configuration.Save(config, @"Config\default.conf");
        }

        /// <summary>
        /// 将波特率列表添加进去
        /// </summary>
        /// <param name="conf"></param>
        private void AddBaudRate(Configuration conf)
        {
            conf.Add("baudRate", baudRateComboBox.Text);
        }

        /// <summary>
        /// 加载配置信息
        /// </summary>
        private bool LoadConfig()
        {
            Configuration config = Configuration.Read(@"Config\default.conf");

            if (config == null)
            {
                return false;
            }

            // 获取波特率
            string baudRateStr = config.GetString("baudRate");
            baudRateComboBox.Text = baudRateStr;

            // 获取奇偶校验位
            int parityIndex = config.GetInt("parity");
            parityComboBox.SelectedIndex = parityIndex;

            // 获取数据位
            int dataBitsIndex = config.GetInt("dataBits");
            dataBitsComboBox.SelectedIndex = dataBitsIndex;

            // 获取停止位
            int stopBitsIndex = config.GetInt("stopBits");
            stopBitsComboBox.SelectedIndex = stopBitsIndex;

            // 获取编码
            int encodingIndex = config.GetInt("encoding");
            encodingComboBox.SelectedIndex = encodingIndex;




            // 窗口状态
            if (config.GetBool("maxmized"))
            {
                this.WindowState = WindowState.Maximized;
            }
            double width = config.GetDouble("windowWidth");
            double height = config.GetDouble("windowHeight");
            double top = config.GetDouble("windowTop");
            double left = config.GetDouble("windowLeft");
            this.Width = width;
            this.Height = height;
            this.Top = top;
            this.Left = left;

            // 面板显示状态
            if (config.GetBool("serialPortConfigPanelVisible"))
            {
                serialSettingViewMenuItem.IsChecked = true;
                serialPortConfigPanel.Visibility = Visibility.Visible;
            }
            else
            {
                serialSettingViewMenuItem.IsChecked = false;
                serialPortConfigPanel.Visibility = Visibility.Collapsed;
            }


            if (config.GetBool("serialCommunicationConfigPanelVisible"))
            {
                serialCommunicationSettingViewMenuItem.IsChecked = true;
                serialCommunicationConfigPanel.Visibility = Visibility.Visible;
            }
            else
            {
                serialCommunicationSettingViewMenuItem.IsChecked = false;
                serialCommunicationConfigPanel.Visibility = Visibility.Collapsed;
            }
            for (int i = 1; i < Tb_bak.Length + 1; i++)
            {
                string str = "CMD" + i;
                Tb_bak[i-1] = config.GetString(str);
            }

            CMD1_TB.Text = Tb_bak[0];
            CMD2_TB.Text = Tb_bak[1];
            CMD3_TB.Text = Tb_bak[2];
            CMD4_TB.Text = Tb_bak[3];
            CMD5_TB.Text = Tb_bak[4];
            CMD6_TB.Text = Tb_bak[5];
            CMD7_TB.Text = Tb_bak[6];
            CMD8_TB.Text = Tb_bak[7];
            CMD9_TB.Text = Tb_bak[8];
            CMD10_TB.Text = Tb_bak[9];
            CMD11_TB.Text = Tb_bak[10];
            CMD12_TB.Text = Tb_bak[11];

            return true;
            
        }
        #endregion
    }
}
