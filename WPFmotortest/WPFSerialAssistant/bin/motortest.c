
//发送函数
void Communication_App(void)
{
	send_buf[0] = 0xa1;
	/*********实时数据**********/
	send_buf[1] = Motor_flag; //电机状态
	send_buf[2] = (uint8_t)Speed;	 //实时速度 同时对应ch1
	send_buf[3] = (uint8_t)(Speed>>8);
	send_buf[4] = (uint8_t)I_d;  //d轴电流
	send_buf[5] = (uint8_t)(I_d>>8);
	send_buf[6] = (uint8_t)I_q;   //q轴电流
	send_buf[7] = (uint8_t)(I_q>>8);
	/*********参数设置**********/
	send_buf[8] = (uint8_t)Kp;
	send_buf[9] = (uint8_t)(Kp>>8);
	send_buf[10] = (uint8_t)Ki;
	send_buf[11] = (uint8_t)(Ki>>8);
	send_buf[12] = (uint8_t)I_kp;
	send_buf[13] = (uint8_t)(I_kp>>8);
	send_buf[14] = (uint8_t)I_ki;
	send_buf[15] = (uint8_t)(I_ki>>8);
	send_buf[16] = (uint8_t)Speed_Ref;
	send_buf[17] = (uint8_t)(Speed_Ref>>8);
	send_buf[18] = (uint8_t)(Umax);
	send_buf[19] = (uint8_t)(Umax>>8);
	//实时曲线显示 数据为16位
	send_buf[20] = 0;   //ch2
	send_buf[21] = 0;		
	send_buf[22] = 0;		//ch3
	send_buf[23] = 0;
	send_buf[24] = 0;		//ch4
	send_buf[25] = 0;
	send_buf[26] = 0;
	send_buf[27] = 0;
	send_buf[28] = 0;
	send_buf[29] = CRC_check(send_buf,29);
	send_buf[30] = 0x78;
	send_buf[31] = 0x56;
}

//CRC
uint8_t CRC_check(uint8_t *Buf, uint8_t len)
{
	uint32_t sum=0;
	uint8_t i = 0;
	for(i = 0; i < len; i++)
	{
		sum += Buf[i];
	}
	return (uint8_t)(sum);
}

